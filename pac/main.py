from classes import *
from nsga2 import *
from actions import *
from a_star import *
from maze.mazeGen1 import genMaze
from random import randint
from copy import deepcopy

#sped
pacManSpeed = 5
#value used for check next location. See actions.py  pac.canMoves()
pacManEpsilon = 2
#generated MazeMap
generatedMazeMap=0
#Map contain pos and True if at pos is an obstacle
pacMap = []
#game windows
WIDTH = 800
HEIGHT = 620
#initial pacman coordonates
pacX=0
pacY=0
#enemy init coord
enemX=0
enemY=0
#blocks dimensions
widhB=20
heightB=20

#graph of intersection points 
graph = {}

#list of tuples representing x and y coordinates of graph nodes in the maze
coordList = []

#returns true if the two points can be neighbors in a graph
def areNeighbourNodes(firstPoint, secondPoint, maze):
    x_offset = -160
    y_offset = 20
    divFactor = 20
    fP = ((firstPoint[0] + x_offset)/divFactor, (firstPoint[1] + y_offset) / divFactor)
    sP = ((secondPoint[0] + x_offset)/divFactor, (secondPoint[1] + y_offset) / divFactor)
    if fP == sP:
        return False
    if fP[1] == sP[1]:
        if fP[0] < sP[0]:
            if '|' in maze[fP[1]][fp[0]:sp[0]]:
                return False 
            else:
                return True
        else:
            if '|' in maze[fP[1]][sP[0]:fP[0]]:
                return False
            else:
                return True
    elif fP[0] == sP[0]:
        if fP[1] > sP[1]:
            for i in range(sP[1],fP[1]):
                if maze[i][sP[0]] == '|':
                    return False
        else:
            for i in range(fP[1],sP[1]):
                if maze[i][sP[0]] == '|':
                    return False
        return True

#margin obstacles
def CreateMarginsObstacles(m_group,obst_grup):
    for i in range(1,5):
        if i == 1:
            obstacleMargin = Obstacle(0, 0)
            obstacleMargin.set_dimensions(WIDTH, 5)
        elif i == 2:
            obstacleMargin = Obstacle(0, 0)
            obstacleMargin.set_dimensions(5, HEIGHT)
        elif i == 3:
            obstacleMargin = Obstacle(0, HEIGHT-5)
            obstacleMargin.set_dimensions(WIDTH, 5)
        else:
            obstacleMargin = Obstacle(WIDTH-5, 5)
            obstacleMargin.set_dimensions(5, HEIGHT)
        obst_grup.add(obstacleMargin)
        m_group.add(obstacleMargin)


#this function create an obstacle map
#also creates the graph to be used for A* and the NSGA2
def CreateObstacleMap(main_group):
    obstacleGroup=pygame.sprite.Group()

    CreateMarginsObstacles(main_group,obstacleGroup)

    ind_x=160
    ind_y=-20;
    global pacX,pacY
    pacX=ind_x+widhB
    pacY=ind_y+heightB

    #generate random maze
    global  generatedMazeMap
    generatedMazeMap=genMaze()
    # print generatedMazeMap

    global graph
    global coordList
    mazeLines = str(generatedMazeMap).splitlines()

    for i in range(0, len(mazeLines)):
        s = mazeLines[i][:16]
        mazeLines[i] = s + s[::-1]

    currentNode = 0

    lineCount=0
    altLineCount = 0
    for line in str(generatedMazeMap).splitlines():
        lineCount+=1
        s = line[:16]
        auxLine = s+s[::-1]
        ind_x = 160

        for char in range(0,len(auxLine)):
            if lineCount >= 15 and lineCount <=17 and char >= 14 and char <= 17:
                #set enemy coordonates
                global enemX
                global enemY
                enemX=ind_x
                enemY=ind_y
            else:
                if auxLine[char]=='|':
                    #add obstacle
                    obstacle = Obstacle(ind_x, ind_y)
                    obstacle.set_dimensions(widhB, heightB)
                    main_group.add(obstacle)
                    obstacleGroup.add(obstacle)
                else:
                    #set pacMap
                    global  pacMap
                    pacMap.append((ind_x,ind_y,True))
                    #check if current point can be a node in the graph
                    #this means it is a corner or intersection on the maze  
                    #check if current point is corner or intersection
                    if (mazeLines[altLineCount-1][char] == '.' or mazeLines[altLineCount+1][char] == '.'):
                        if (mazeLines[altLineCount][char-1] == '.' or mazeLines[altLineCount][char+1] == '.'):
                            newCoord = (ind_x, ind_y)
                            graph[currentNode] = []
                            for i in range(0, len(coordList)):
                                if newCoord[0] == coordList[i][0] or newCoord[1] == coordList[i][1]:
                                    if areNeighbourNodes(newCoord, coordList[i], mazeLines):
                                        graph[i].append(currentNode)
                                        graph[currentNode].append(i)
                            coordList.append(newCoord)
                            currentNode+=1

            ind_x+=20
        ind_y+=20
        altLineCount+=1

    # for key in graph:
    #     print key, graph[key]
    # print('\n'.join('{}: {}'.format(*k) for k in enumerate(coordList)))
    # a_star(0, len(graph)-1, graph, coordList)

    return obstacleGroup
#This function add to map, food for pacman
def addPacFood(main_group):
    foodGroup=pygame.sprite.Group()
    global pacMap

    #big points
    bigpoint1=random.randint(1,len(pacMap))
    bigpoint2=random.randint(1,len(pacMap))
    while bigpoint2 == bigpoint1:
    	bigpoint2=random.randint(1,len(pacMap))
    bigpoint3=random.randint(1,len(pacMap))
    while bigpoint2 == bigpoint1 or bigpoint2 == bigpoint1:
    	bigpoint3=random.randint(1,len(pacMap))
    #apple
    apple=random.randint(1,len(pacMap))
    while apple == bigpoint1 or apple == bigpoint2 or apple == bigpoint3:
    	apple=random.randint(1,len(pacMap))

    for i in range(0,len(pacMap)):
        val=0
        str1 = ''
        str2 = ''
        if i == apple:
			val=700#from pacman wiki
			str1='../resources/png/20x20/apple1.png'
			str2='../resources/png/20x20/apple2.png'
        elif i == bigpoint1 or i == bigpoint2 or i == bigpoint3:
            val=50
            str1='../resources/png/20x20/pointBig.png'
            str2='../resources/png/20x20/pointBig.png'
        else:	
            val=10
            str1='../resources/png/20x20/point.png'
            str2='../resources/png/20x20/point1.png'
        food = Food(pacMap[i][0], pacMap[i][1], str1,str2,val,main_group)
        main_group.add(food)
        foodGroup.add(food)
    return foodGroup

def secondElem(elem):
	return ((1.0/elem[1])**2 + elem[2] ** 2) ** 0.5

def main():
    FPS = 40;
    pygame.init()

    #music :)
    # pygame.mixer.music.load('../resources/music/music.ogg')
    # pygame.mixer.music.set_volume(0.3)
    # pygame.mixer.music.play(-1)

    # resolution is fixed
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_icon(pygame.image.load('../resources/png/20x20/p2a.png'))#set windows icon
    pygame.display.set_caption("PacMan")
    bg = pygame.image.load("../resources/png/background.png")

    # add it to group
    main_group = pygame.sprite.Group()

    # add the obstacles to an obstacle group and to main group
    obstacleGroup = CreateObstacleMap(main_group)

    #just for testing
    enemyGroup=pygame.sprite.Group()
    enemy1 = Enemy(enemX-20, enemY-20, '../resources/png/20x20/ghost1.png', '../resources/png/20x20/ghost2.png', obstacleGroup)
    enemyGroup.add(enemy1)

    #create foods
    foodGroup=addPacFood(main_group)

    global coordList
    global graph
    for key in graph:
    	print key, graph[key]
    noOfPacs = (len(graph) - 1)/2

    # list of 3 element tuples
    # 0 - path
    # 1 - score
    # 2 - time
    obtainedObjectives = []

    # create new instance of pacman      !!!!!!!!!!! Aded enemyGroup. You know for what.!!!!!!!!!!!
    pac = PacMan(pacX, pacY+20, '../resources/png/20x20/p1a.png', '../resources/png/20x20/p2a.png', obstacleGroup,enemyGroup,foodGroup)

    # initial population
    destinations = initPopulation(noOfPacs)

    maxScore = 0.0
    for item in foodGroup:
    	maxScore+=item.getValue()


    # create the paths to take by the first generation
    paths = []
    for i in range(0, noOfPacs):
    	paths.append(a_star(0, destinations[i], graph, coordList))

    # best scores obtained
    obtainedScore = 0.0
    bestPath = []
    bestTime = 1.0

    index = 0
    mutationChance = 0.1
    print maxScore


    # Repeat the simulations until the best score is at least 85% of the maxScore
    while (obtainedScore*100.0)/maxScore <85.0 and index<150:
    	print "index = ", index
    	index = index+1
    	print len(paths)
    	i = 0
    	auxFood = deepcopy(foodGroup)
    	for i in range(0, len(paths)):
    		# print i
    		copyOfFood = pygame.sprite.Group(auxFood)
    		copyOfEnemy = pygame.sprite.Group(enemyGroup)
    		testPac = PacMan(pacX, pacY+20, '../resources/png/20x20/p1a.png', '../resources/png/20x20/p2a.png', obstacleGroup, copyOfEnemy, copyOfFood)
    		currentScore, currentTime = individualSimulation(testPac, paths[i], FPS/4.0, pacManSpeed*4, coordList)
    		obtainedObjectives.append((paths[i], currentScore, -currentTime))

    	
    	front = makeParetoFront(obtainedObjectives)
    	front.sort(key = secondElem, reverse = True)
    	if front[0][1] > obtainedScore:
    		if mutationChance > 0.15:
    			# decrease the mutation chance, on the premise that the gene pool is becoming more diverse
    			mutationChance-=0.15
    			if mutationChance <0.15:
    				mutationChance = 0.15
    		obtainedScore = front[0][1]
    		bestPath = front[0][0]
    		bestTime = -front[0][2]
    		print -front[0][2]
    		print obtainedScore
    		print bestPath
    	else:
    		print front[0][1]
    		print -front[0][2]
    		if mutationChance<0.97:
    			# increase the mutation chance, on the premise that the gene pool is becoming less diverse
    			mutationChance += 0.05

    	#clear all duplicate items
    	frontAux = []
    	for k in range(0, len(front)):
    		ok = True
    		for p in range(0, len(frontAux)):
    			if frontAux[p][1:] == front[k][1:]:
    				ok = False
    		if ok:
    			frontAux.append(front[k])
    	front = frontAux

    	# Be a dictator and have those pacMen and pacWomen make babies or kill eachother trying :))
    	paths = startBreedingProcess(front, mutationChance, graph, coordList)
    	if len(paths)>50:
    		paths = paths[:50]



    print "Front: "
    for item in front:
    	print item[1], item[2] 

    print len(obtainedObjectives)
    print len(front)
    if front == []:
    	print 'Void front'
    	exit()

    main_group.add(pac,enemy1)


    # clock for game
    clock = pygame.time.Clock()

    # wait for input before starting the amazing demonstration of the highest scoring pacman (not with the best time, though)
    raw_input("Start display")

    # offset for calculating time
    offset = pygame.time.get_ticks()
    myfont = pygame.font.SysFont("monospace", 15)

    ##########################################################################################
    """NOTE: - the board won't have all the food sprites on it because they all change
    		   their image through the simulations
    		 - the sprites still exist, they just can't be seen :))
    		 - this should be fixed (although i don't think the Godfather would mind :))) )"""
	##########################################################################################	 

    while True:
        # put background
        screen.blit(bg, (pacX, pacY))

        main_group.draw(screen)

        # pacMan moves by himself for as long as he still knows where to go
        if bestPath == []:
	        # controls actions
	        process(pac, FPS, 0)
        else:
        	# choose pacMan's action automatically
	    	autoProcess(pac, FPS, 0, bestPath, coordList)
	    	timePassed = pygame.time.get_ticks() - offset
	    	timeLabel = myfont.render('time = '+str(timePassed/1000.0), 1, (255, 255, 0))

        pac.pacManMove(pacManSpeed)  # last is speed

        enemy1.simple_motion(3)

        pygame.display.flip()

        #score label
        pygame.draw.rect(screen,(0,0,0),(20,20,140,100))

        label = myfont.render('score = '+str(pac.score), 1, (255, 255, 0))#('coords: ' + str(pac.get_rect().x) + ' ' + str(pac.get_rect().y), 1, (255, 255, 0))
        screen.blit(label, (20, 20))
        screen.blit(timeLabel, (20, 40))	

        # animation
        main_group.update()

        # simulate fps
        clock.tick(FPS)


if __name__ == '__main__':
    main()








#idei & probleme :
#----new-----
#Imbunatatire clasa enemy
