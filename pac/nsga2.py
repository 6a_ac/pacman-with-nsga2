#this file contains functions for implementing the NSGA2 

from classes import *
from actions import *
from a_star import *
from random import randint
import random

file = open('debug.txt', 'w')

# returns a list of noOfPacs destinations (NOT PATHS!!!) generated pseudo-randomly
def initPopulation(noOfPacs):
	destinations = []
	for i in range(0, noOfPacs):
		aux = randint(0, noOfPacs)
		while aux in destinations:
			aux = randint(0, noOfPacs)
		destinations.append(aux)

	return destinations

# check if objective1 can be on the pareto front when compared to objective2
def paretoCheck(objective1, objective2):
	if (objective1[1] <= objective2[1] and objective1[2] < objective2[2]) or (objective1[1] < objective2[1] and objective1[2] <= objective2[2]):
		return False
	return True

#returns the pareto Front
def makeParetoFront(objectiveList):

	paretoFront = []
	for i in range(0, len(objectiveList)):
		ok = True
		for j in range(0, len(objectiveList)):
			if i<>j:
				if paretoCheck(objectiveList[i], objectiveList[j]) is False:
					ok = False
		if ok is True:
			paretoFront.append(objectiveList[i])

	return paretoFront

#does a simulation for the given pacMan that follows the given path
#simulates time passing by adding 1.0/FPS on every iteration
#returns time and score
def individualSimulation(pac, path, FPS, speed, coordList):
	timePassed = 0
	score = 0
	internalPath = list(path)
	while internalPath<>[]:
		autoProcess(pac, FPS, 0, internalPath, coordList)
		timePassed = timePassed + 1.0/FPS
		pac.pacManMove(speed)

	return (pac.score, timePassed)

#breeds two paths together
#!!!NO LONGER APPLICABLE!!! takes a random number of nodes from the mother path, a random number of nodes from the father path and it combines them
#mutation: it will insert a new random node in the path (could be good, could be bad)
#stick two paths together and mutates them
def breed(motherPath, fatherPath, mutationChance, breedingChance, graph, coordList):
	motherGenes = randint(0, len(motherPath)-1) #len(motherPath)-1 
	fatherGenes = randint(0, len(fatherPath)-1) #0
	breed = random.random()
	mutation = random.random()
	auxPath = []
	if breed < breedingChance:
		interm = a_star(motherPath[motherGenes], fatherPath[fatherGenes], graph, coordList)
		auxPath = motherPath[:motherGenes] + interm + fatherPath[(fatherGenes):]

		#These prints were used for debugging - DO NOT DELETE (just in case)
		# print "Breed"
		# print motherPath[:motherGenes]
		# print interm
		# print fatherPath[(fatherGenes+1):]
		# print auxPath
	else:
		check = random.random()
		if check<0.5:
			auxPath = motherPath
		else:
			auxPath = fatherPath

	if mutation <= mutationChance:
		# file.write("Before mutation: {0}\n".format(auxPath))
		mutationGene = randint(0, len(auxPath) - 1)
		mutationValue = randint(0, len(graph) - 1)

		# force the mutation towards a new node in the graph to speed up solution finding
		if mutationChance >= 0.92:
			while mutationValue in auxPath:
				if mutationValue < len(graph) - 1:
					mutationValue+=1
				else:
					mutationValue = 0

		leftPath = auxPath[:mutationGene] + a_star(auxPath[mutationGene], mutationValue, graph, coordList)
		rightPath = []
		if mutationGene < len(auxPath) - 1:
			rightPath = a_star(mutationValue, auxPath[mutationGene], graph, coordList) + auxPath[(mutationGene+1):]
		auxPath = leftPath + rightPath
		# file.write("Mutation: {0}\n".format(auxPath))
		# file.write("Left: {0}\n".format(leftPath))
		# file.write("Right: {0}\n".format(rightPath))
		# for i in range(0, len(auxPath)):
		# 	file.write(str(auxPath[i]) + " " + str(coordList[auxPath[i]]) + "\n")
		# raw_input()

	# clear the path of duplicates
	i = 0
	while i<len(auxPath)-1:
		if auxPath[i] == auxPath[i+1]:
			del auxPath[i+1]
		else:
			i+=1

	# if mutation<=mutationChance:
	# 	file.write("Computed Mutation: {0}\n".format(auxPath))
	# 	# raw_input()

	return auxPath

#returns new population
#kind of a communist and hunger games style function :))
#keeps the elite alive
#the front must be sorted based on something (score in this case)
def startBreedingProcess(front, mutationChance, graph, coordList):
	newPopulation = list()
	# elitism
	if len(front) > 3:
		newPopulation.append(front[0][0])
		newPopulation.append(front[1][0])
		newPopulation.append(front[2][0])

	for i in range(3 ,len(front)):
		# file.write("Breeding {0}\n".format(i))
		mother = randint(0, len(front) - 1)
		father = randint(0, len(front) - 1)
		while mother == father:
			father = randint(0, len(front) - 1)
		newPopulation.append(breed(front[mother][0], front[father][0], mutationChance, 0.85, graph, coordList))

	return newPopulation