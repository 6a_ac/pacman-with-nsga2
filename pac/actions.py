import pygame, sys, classes, random
from main import pacManSpeed
from main import pacManEpsilon

def goUp(pac):
    if pac.canMoves(0,-(pacManSpeed+pacManEpsilon))==True:
        pac.stop_motion()
        pac.up=True   #up

def goDown(pac):
    if pac.canMoves(0,(pacManSpeed+pacManEpsilon))==True:
        pac.stop_motion()
        pac.down = True  # down 

def goLeft(pac):
    if pac.canMoves(-(pacManSpeed+pacManEpsilon),0)==True:
        pac.stop_motion()
        pac.left=True   #left

def goRight(pac):
    if pac.canMoves((pacManSpeed+pacManEpsilon),0)==True:
        pac.stop_motion()
        pac.right = True  # right

def autoProcess(pac, FPS, total_frames, path, coordList):
    #exit
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
    
    currentCoord = (pac.get_rect().x, pac.get_rect().y)

    if path <> []:
        if currentCoord == coordList[path[0]]:
            del path[0]
            if path == []:
                pac.stop_motion()
                return
            nextCoord = coordList[path[0]]
            if currentCoord[0] == nextCoord[0]:
                if currentCoord[1] > nextCoord[1]:
                    goUp(pac)
                else:
                    goDown(pac)
            elif currentCoord[1] == nextCoord[1]:
                if currentCoord[0] > nextCoord[0]:
                    goLeft(pac)
                else:
                    goRight(pac)


def process(pac, FPS, total_frames):

    #exit
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

    keys = pygame.key.get_pressed()

    #treats keys
    if keys[pygame.K_d]:
        goRight(pac)

    if keys[pygame.K_a]:
        goLeft(pac)

    if keys[pygame.K_w]:
        goUp(pac)

    if keys[pygame.K_s]:
        goDown(pac)

