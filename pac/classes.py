import pygame
import math
#conts
left=1
up=2
right=3
down=4

def load_image(name):
    image = pygame.image.load(name)
    return image

class GameUnit(pygame.sprite.Sprite):
    #obstacleGroup acts as a Group of sprites with which the GameUnit can collide with, stopping it from moving as an effect
    def __init__(self,canMov,x,y,imgPath1,imgPath2, obstacleGroup):
        super(GameUnit, self).__init__()
        #if this unit canMoves
        self.canMov=canMov
        #Keep the game area at hand, loading it every time is not very efficient
        self.area = pygame.display.get_surface().get_rect()

        #Obstacles that the gameUnit can hit and it will stop moving
        self.obstacleGroup = obstacleGroup

        #images
        self.images = []
        for i in range(0,10): #second parameter simulate speed of animation
            self.images.append(load_image(imgPath1))
            self.images[i]
        for i in range(0,10):
            self.images.append(load_image(imgPath2))

        self.index = 0#index in images[]
        self.image = self.images[self.index]

        #position
        self.rect=self.image.get_rect()

        #motion
        self.left=False
        self.right=False
        self.up=False
        self.down=False

        #initial position
        self.rect.x=x
        self.rect.y=y

        #image state
        self.imgStat=right

    def update(self):
            self.index += 1
            if self.index >= len(self.images):
                self.index = 0
            self.image = self.images[self.index]

    def destroy(self, ClassName):
        ClassName.List.remove(self)
        GameUnit.allsprites.remove(self)
        del self

    def simple_motion(self, speed):

        if self.canMov == True:
            # Fixed game unit not moving anymore when reaching the edge of the screen (in current context)
            if speed > 0:
                x_mov = 0
                y_mov = 0

                # pacman rotate itself only in motion function, and if the next position is not in collision
                curentPosition = self.imgStat

                if self.left == True:
                    x_mov -= speed;
                    self.rotate_image(left)

                if self.right == True:
                    x_mov += speed;
                    self.rotate_image(right)

                if self.up == True:
                    y_mov -= speed;
                    self.rotate_image(up)

                if self.down == True:
                    y_mov += speed;
                    self.rotate_image(down)

                pos = self.rect
                #My solution for Moving action, with collision . It's simple.
                if self.canMoves(x_mov,y_mov):
                    self.rect = self.rect.move((x_mov, y_mov))

    def canMoves(self,next_x,next_y):
        nextRect=self.rect.copy()
        nextRect.x+=next_x
        nextRect.y+=next_y
        for obstacle in self.obstacleGroup:
            if nextRect.colliderect(obstacle.rect):
                return False
        return True

    def stop_motion(self):
        self.down=False;
        self.up=False;
        self.left=False;
        self.right=False;

    def get_rect(self):
        return self.rect

    def rotate_image(self,direction):
        degrees=0
        if self.imgStat <> direction:
            #calculcate degrees
            degrees=(self.imgStat-direction)*90
            #rotate all
            for index in range(0,len(self.images)):
                self.images[index]=pygame.transform.rotate(self.images[index],degrees)
            self.imgStat=direction

class PacMan(GameUnit):
    List = pygame.sprite.Group()
    def __init__(self, x, y, imgP1,imgP2, obstacleGroup,enemyGroup,foodGroup):
        GameUnit.__init__(self, True,x, y,imgP1,imgP2, obstacleGroup)
        PacMan.List.add(self)
        self.foodGroup=foodGroup
        self.score=0
    def pacManMove(self,speed):
        self.canEat()
        self.simple_motion(speed)

    def canEat(self):
        for food in self.foodGroup:
            if self.rect.colliderect(food.rect):
                self.score += food.getValue()
                food.remove(self.foodGroup)
                food.remove(food.List)
                food.remove(food.mainGroup)

class Obstacle(pygame.sprite.Sprite):
    def __init__(self, coord_x, coord_y):
        pygame.sprite.Sprite.__init__(self)
        self.image = load_image('../resources/png/20x20/Wall1.png');
        self.rect = self.image.get_rect()
        self.area = pygame.display.get_surface().get_rect()
        self.rect.x = coord_x
        self.rect.y = coord_y

        self.initx = coord_x
        self.inity = coord_y

    def get_rect(self):
        return self.rect

    #set dimensions by scaling image and updating the rectangle
    def set_dimensions(self,width,height):
        self.image = pygame.transform.scale(self.image,(width,height))
        self.rect = self.image.get_rect()
        self.rect.x = self.initx
        self.rect.y = self.inity

########################
# Food class #
########################
class Food(GameUnit):
    List = pygame.sprite.Group()
    def __init__(self, x, y, imgP1,imgP2,value,mainDrawGroup):
        GameUnit.__init__(self, True,x, y,imgP1,imgP2,None)
        Food.List.add(self)
        self.value=value
        self.mainGroup=mainDrawGroup
    def setValue(self,val):
        self.value=val
    def getValue(self):
        return self.value

########################
# enemy class #
########################
class Enemy(GameUnit):
    List = pygame.sprite.Group()
    def __init__(self, x, y, imgP1,imgP2, obstacleGroup):
        GameUnit.__init__(self, True,x, y,imgP1,imgP2, obstacleGroup)
        Enemy.List.add(self)
        self.right=True
        self.vulnerable=False#if pac eat an bigPoint the enemy will be vulnerable
    # def naivPacSearch(self,PacMan):
    #     a=abs(PacMan.rect.x-self.rect.x)
    #     b=abs(PacMan.rect.y-self.rect.y)
    #     c=math.sqrt(a^2 +b^2)
            