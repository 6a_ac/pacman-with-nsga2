#This file contains the functions necessary for pathfinding with A*

#adds the node to the list if one with a better heuristic isn't in the list already
def addNode(openList, nodeToAdd):
	for elem in openList:
		if elem[0] == nodeToAdd[0]:
			if (elem[1] + elem[2]) > (nodeToAdd[1] + nodeToAdd[2]):
				openList.remove(elem)
				openList.append(nodeToAdd)
				return
			else:
				return
	openList.append(nodeToAdd)
	

def euclidianDist(tuple1, tuple2):
	return ((tuple1[0] - tuple2[0]) **2 + (tuple2[1] - tuple1[1]) ** 2) ** (0.5)

def listSortFun(elem):
	return elem[1] + elem[2]

#returns a list of value representing the path from the start node to the finish node
def a_star(startNode, finishNode, graph, coordList):
	euristic = euclidianDist(coordList[startNode], coordList[finishNode])

	#openList contains a list of tuples of 3 elements
	#	0 - node
	#	1 - euristic
	#	2 - current distance covered
	#	3 - path to get to the node
	openList = [(startNode, euristic, 0, [startNode])]
	closedList = []
	while openList <> []:
		currentNode = openList[0]
		closedList.append(currentNode[0])
		if currentNode[0] == finishNode:
			#print currentNode[3]
			return currentNode[3]
		neighborList = graph[currentNode[0]]
		for node in neighborList:
			euristic = euclidianDist(coordList[node], coordList[finishNode])
			distanceCovered = currentNode[2] + euclidianDist(coordList[currentNode[0]], coordList[node])
			path = list(currentNode[3])
			path.append(node)
			nodeToAdd = (node, euristic, distanceCovered, path)
			addNode(openList, nodeToAdd)
		
		openList.remove(currentNode)
		openList.sort(key = listSortFun)
		
